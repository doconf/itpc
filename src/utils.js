import axios from 'axios';

const API_URL = 'https://code-challenge-server-itpc.herokuapp.com/';

class Utils {
  fetch = (endpoint) => {
    return axios
      .get(`${API_URL}${endpoint}`, {
        headers: { 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization, cors, Access-Control-Allow-Origin', 'cors': 'true', 'Access-Control-Allow-Origin': '*', 'Cache-Control': 'no-cache' }
      });
  }

  store = (endpoint, data) => {
    return axios
      .post(`${API_URL}${endpoint}`, data, {
        headers: { 'access-control-allow-origin': '*', 'Cache-Control': 'no-cache' }
      });
  }

  update = (endpoint, data) => {
    return axios
      .put(`${API_URL}${endpoint}`, data, {
        headers: { 'access-control-allow-origin': '*', 'Cache-Control': 'no-cache' }
      });
  }

  delete = (endpoint) => {
    return axios
      .delete(`${API_URL}${endpoint}`, {
        headers: { 'access-control-allow-origin': '*', 'Cache-Control': 'no-cache' }
      });
  }

}

let utils = new Utils();
export default utils;
