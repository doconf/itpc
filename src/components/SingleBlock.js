import React, { Component } from 'react';
import utils from '../utils';
import '../App.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import LoadingComponent from "./loading";

class SingleBlock extends Component {
	constructor(props) {
		super(props);
		this.state = {
			res: {},
			isLoading: true
		}
	}

	componentDidMount() {
		let block_hash = '0000000000000bae09a7a393a8acded75aa67e46cb81f7acaa5ad94f9eacd103'
		utils.fetch('rawblock/'+ block_hash)
			.then(res => {
				console.log("res -- ", res)
				this.setState({res: res.data, isLoading: false});
			})
			.catch(err => {
				console.log("err -- ", err)
				this.setState({res: {Error: "Can't get data from the server"}, isLoading: false});
			});
	}

	render() {
		return (
			<div>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell><b>Key</b></TableCell>
							<TableCell align="left"><b>Value(s)</b></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{this.state.isLoading ? <LoadingComponent /> : null}
						{Object.keys(this.state.res).map((key, index) => {
							return (
							<TableRow key={index}>
								<TableCell component="th" scope="row"><b>{key}</b></TableCell>
								<TableCell align="left">
									{ Array.isArray(this.state.res[key])
									? (this.state.res[key].map((value, i) => {
										return value && typeof value === 'object' && value.constructor === Object
										// 	? (Object.keys(value).map((innerval, counter) => {
											? <pre key={i} className="txtTable">{JSON.stringify(value, undefined, 5)}</pre>
										// 		}))
											: <div key={i}>{value.toString()}</div>
										
										// return <div key={i}>{value.toString()}</div>
									}))
									: this.state.res[key].toString()
									}
								</TableCell>
							</TableRow>
							)
						})}
					</TableBody>
				</Table>
			</div>
		);
	}
}

export default SingleBlock;
