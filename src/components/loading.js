import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
// Import Style
import '../App.css';

export default class LoadingComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
      return (
        <div className="loadingContainer"> 
            <div className="loadingBlock"> 
                <CircularProgress size={50} status="loading" style={{transform:"none"}}/>
            </div>
        </div>
      );
    }
}

