import fetch from 'isomorphic-fetch';
export const API_URL = 'https://blockchain.info/';

class Utils {
  httpRequest(endpoint, method, data, cb) {
    const host = API_URL;
    var url = host + endpoint;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open(method, url, true);
    xmlhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xmlhttp.setRequestHeader('cors', true);
    xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers');
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState === 4) {
        // console.log(" ---------------- ");
        console.log("xmlhttp ", xmlhttp);
        // console.log("xmlhttp.status ", xmlhttp.status);
        // console.log(" ---------------- ");
        if (xmlhttp.status !== 0) {
          if (xmlhttp.response !== "") {
            let response = JSON.parse(xmlhttp.response);

            cb && cb(response);
          }
        } else {
          let response = { status: false, result: { message: "Connection Refused" } };
          cb && cb(response);
        }

      }
      xmlhttp.send(JSON.stringify(data));
    }
  }

  callApi(endpoint, method = 'get', body) {
    console.log("input req -- ", endpoint)
    return fetch(`${API_URL}${endpoint}`, {
        headers: { 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization', 'Access-Control-Allow-Origin': '*', 'content-type': 'application/json', 'cors': 'true' },
        method,
        body: JSON.stringify(body),
      })
      .then(response => response.json().then(json => ({ json, response })))
      .then(({ json, response }) => {
        console.log("res -- ", json, response)
        if (!response.ok) {
          return Promise.reject(json);
        }

        return json;
      })
      .then(
        response => response,
        error => error
      );
  }

}

var utils = new Utils();
export default utils;
